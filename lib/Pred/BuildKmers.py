from __future__ import division
import multiprocessing
import logging
from MakeIndexes import ts_tv_rate
from MakeIndexes import utr_cds_rate
from MakeIndexes import snp_density
from MakeIndexes import measure_snp_eff
from subprocess import Popen, PIPE
import tabix
import sys
import math


class BuildKmersThread(multiprocessing.Process):
    def __init__(self, thread_id, chr_info, ref_table, ind_table, genes_table, km_length, ovp,
                 index_dict, tot_chrom, counter, global_titv):
        multiprocessing.Process.__init__(self)
        self.thread_id = thread_id
        self.chr_info = chr_info
        self.ref_table = ref_table
        self.ind_table = ind_table
        self.genes_table = genes_table
        self.km_length = km_length
        self.ovp = ovp
        self.indexes = index_dict
        #self.dil_values = dil_values
        self.ref_tb = tabix.open(ref_table)
        self.ind_tb = tabix.open(ind_table)
        self.genes_tb = tabix.open(genes_table)
        self.tot_chrom = tot_chrom
        self.counter = counter
        self.global_titv = global_titv

    def run(self):
        self.build_kmers(self.chr_info,
                         self.km_length,
                         self.ovp,
                         self.indexes,
                         #self.dil_values,
                         self.tot_chrom,
                         self.counter,
                         self.global_titv)


    def build_kmers(self, chr_info, km_length, ovp, indexes, total_chr, counter, global_titv):

        chr_names = sorted(chr_info.keys())
        for chr_name in chr_names:

            bg = 1
            end = km_length
            chr_size = int(chr_info[chr_name])
            kmer_flag = 0

            while kmer_flag == 0:

                in2 = 0
                ref_vars = []
                ind_vars = []
                gene_feat = []

                # trim the kmer if it ends beyond the limits of the chromosome.
                if end <= chr_size:
                    k_size = km_length

                else:
                    k_size = chr_size - bg + 1
                    end = chr_size
                    kmer_flag = 1

                try:
                    # get variants falling in the current kmer for reference individual
                    ref_vars = list(self.ref_tb.query(chr_name, bg, end))

                except tabix.TabixError:
                    pass

                try:
                    # get variants falling in the current kmer for non reference individuals
                    ind_vars = list(self.ind_tb.query(chr_name, bg, end))

                except tabix.TabixError:
                    pass

                try:
                    # get gene features falling in the current kmer
                    gene_feat = list(self.genes_tb.query(chr_name, bg, end))

                except tabix.TabixError:
                    pass

                n_ref_vars = sum(1 for _ in ref_vars)
                n_ind_vars = sum(1 for _ in ind_vars)
                n_gene_feat = sum(1 for _ in gene_feat)

                # check if the current kmer falls in a gene region
                if n_gene_feat != 0:
                    region_type = "g"
                else:
                    region_type = "i"

                if n_ind_vars != 0 or n_ref_vars != 0:
                    # SNP density in reference variants
                    if n_ref_vars != 0:
                        in1 = snp_density(n_ref_vars, k_size)
                    else:
                        in1 = 0

                    # ti/tv rate
                    if n_ind_vars != 0:
                        ts_tv_ind = ts_tv_rate(ind_vars)
                        in2 = abs(global_titv - ts_tv_ind)

                    else:
                        in2 = 0

                    # utr_den/cds_den rate in individual variants
                    if n_gene_feat != 0 and n_ind_vars != 0:
                        g_in1 = utr_cds_rate(gene_feat, ind_vars, bg, end)
                    else:
                        g_in1 = 0

                    # sum of the variants effect over the current kmer
                    if n_ref_vars != 0:
                        snpeff_ivs = measure_snp_eff(ref_vars, k_size)
                    else:
                        snpeff_ivs = 0

                    if n_ind_vars != 0:
                        snpeff_mivs = measure_snp_eff(ind_vars, k_size)
                    else:
                        snpeff_mivs = 0

                    g_in2 = (snpeff_mivs + snpeff_ivs) / k_size

                    # store all the indexes in an array
                    if region_type == "g":
                        ind_values = [in1, in2, g_in1, g_in2]
                    else:
                        ind_values = [in1, in2, None, None]

                else:
                    if region_type == "g":
                        ind_values = [0, 0, 0, 0]
                    else:
                        ind_values = [0, 0, None, None]

                # store the indexes for the current kmer
                indexes[(chr_name, bg, end)] = ind_values

                # move to the next kmer
                bg = (end - ovp + 1)
                end = bg + km_length - 1
            counter.increment()
            tot = (counter.value / float(total_chr))*100

            if tot % 5 < 0.05:
                logging.info(str(math.trunc(tot))+"% of the chromosomes completed ..")
