"""
Created on 10-08-2015

@author: Diego Diaz
"""
from operator import itemgetter
from sys import exit


def snp_density(variants, km_length):
    snp_den = variants / float(km_length)
    return snp_den


def ts_tv_rate(variants):

    n_ti = 0
    n_tv = 0

    variants = list(variants)

    for var in variants:

        if (var[2] == "A" and var[3] == "G") or (var[2] == "G" and var[3] == "A") or (
                var[2] == "C" and var[3] == "T") or (var[2] == "T" and var[3] == "C"):
            n_ti += 1

        else:
            if (var[2] == "A" and var[3] == "C") or (var[2] == "C" and var[3] == "A") or (
                    var[2] == "T" and var[3] == "G") or (var[2] == "G" and var[3] == "T") or (
                    var[2] == "T" and var[3] == "A") or (var[2] == "A" and var[3] == "T") or (
                    var[2] == "C" and var[3] == "G") or (var[2] == "G" and var[3] == "C"):
                n_tv += 1

    if n_tv == 0:
        rate = 0
    else:
        rate = n_ti / float(n_tv)

    return rate


def check_overlap(coords1, coords2):
    """
    check if two segments are overlapped
    :param coords1:
    :param coords2:
    :return:
    """
    if coords1[0] <= coords2[0] <= coords1[1]:
        return True
    else:
        if coords1[0] <= coords2[1] <= coords1[1]:
            return True
        else:
            if coords2[0] <= coords1[0] <= coords2[1]:
                return True
            else:
                if coords2[0] <= coords1[1] <= coords2[1]:
                    return True

    return False


def merge_overlapped(utrs):
    """
    for a given set of genome segments, merge those that overlap between them
    :param utrs:
    :rtype : list
    """
    if len(utrs) <= 1:
        return utrs

    outer_flag = 0
    curr = 0
    nxt = curr + 1
    nxt_itm_flag = 0

    # iterate over all segments
    while outer_flag == 0:

        arr_len = len(utrs)
        inner_flag = 0

        # compare all segments one each other
        while inner_flag == 0:

            # if two segments overlap, merge them, replace the current segment with
            # the new merged one and restart the comparison between this merged
            # segment against all the others

            if check_overlap(utrs[curr], utrs[nxt]):
                nxt_itm_flag = 1
                tot_reg = utrs[curr] + utrs[nxt]
                merged_reg = [min(tot_reg), max(tot_reg)]
                utrs[curr] = merged_reg
                utrs.pop(nxt)
                arr_len = len(utrs)
                inner_flag = 1

            if inner_flag == 0:
                nxt += 1
            # condition to end the comparison
            if nxt > (arr_len - 1) and inner_flag == 0:
                inner_flag = 1
                nxt_itm_flag = 0

        # condition to move to the next segment
        if nxt_itm_flag == 0:
            curr += 1
            nxt = curr + 1
        # condition to finish the merging
        if curr == (arr_len - 1):
            outer_flag = 1

    return utrs


def get_length(coords):
    """
    get the sum of the lengths of a set of genome segments
    :param coords:
    :return tot_length:
    """
    tot_len = 0
    for coord in coords:
        tmp = coord[1] - coord[0]
        tot_len += tmp

    return tot_len


def crop_bounds(segments, bg_bound, end_bound):
    """
    crop those genome segments that are extended beyond
    the limits of the kmer where they are been analyzed
    :param segments:
    :param bg_bound:
    :param end_bound:
    :return:
    """
    for pos in range(0, len(segments)):

        if segments[pos][0] < bg_bound:
            segments[pos][0] = bg_bound

        if segments[pos][1] > end_bound:
            segments[pos][1] = end_bound

    return segments


def utr_cds_rate(gene_feats, ind_vars, bg_km, end_km):
    """
    measure the (cds density / utr density) rate for a given window in the genome
    :param gene_feats:
    :param ind_vars:
    :return:
    """

    # number of variants in utrs and cds respectively
    n_utrs = 0
    n_cds = 0

    # arrays containing the features
    utrs = []
    cds = []

    # split features according their type (CDS, 3utr or 5utr)
    for feat in gene_feats:
        feat_class = feat[6].lower()
        if "cds" in feat_class:
            cds.append([int(feat[1]),int(feat[2])])
        else:
            if "utr" in feat_class:
                utrs.append([int(feat[1]), int(feat[2])])

    # crop regions to the bounds of kmer window
    utrs = crop_bounds(utrs, bg_km, end_km)
    cds = crop_bounds(cds, bg_km, end_km)

    # merge overlapped regions (to avoid counting variants more than once)
    utrs = merge_overlapped(utrs)
    cds = merge_overlapped(cds)

    # get the total length spanned by utr and cds regions
    utrs_len = get_length(utrs)
    cds_len = get_length(cds)

    # iterate over variants
    for snp in ind_vars:
        # count the number of snps that falls in utr regions
        for utr in utrs:
            if utr[0] <= int(snp[1]) <= utr[1]:
                n_utrs += 1

        # count the number of snps that falls in cds regions
        for cod in cds:
            if cod[0] <= int(snp[1]) <= cod[1]:
                n_cds += 1

    # get snp density in utr regions
    if utrs_len != 0:
        utr_den = n_utrs / float(utrs_len)
    else:
        utr_den = 0

    # get snp density in cds regions
    if cds_len != 0:
        cds_den = n_cds / float(cds_len)
    else:
        cds_den = 0

    # get the cds_den / utr_den ratio
    if utr_den != 0:
        final_den = cds_den / float(utr_den)
    else:
        final_den = 0

    return final_den


def measure_snp_eff(variants, km_length):
    """
    measure the "snp effect density" in a segment of the genome of length km_length"
    :param variants:
    :param km_length:
    :return:
    """
    sum_eff = 0
    for pos in variants:
        if pos[4] == "MODIFIER":
            sum_eff += 1
        else:
            if pos[4] == "MODERATE":
                sum_eff += 4
            else:
                if pos[4] == "LOW":
                    sum_eff += 2
                else:
                    if pos[4] == "HIGH":
                        sum_eff += 8
                    else:
                        sum_eff += 0
    return sum_eff
