#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

struct LinkedList{
	char * word;
	int n_elements;
	struct LinkedList * next;
};

struct LinkedList * get_pos(struct LinkedList * list, int pos){
	
	int curr_pos = 0;
	struct LinkedList *tmp = list;

	while(1){
		if(curr_pos == pos){
			break;
		}
		tmp = tmp->next;
		curr_pos++;
	}
	return tmp;
}

struct LinkedList * strsplit(char* a_str, const char a_delim){

	struct LinkedList *fields = (struct LinkedList*)malloc(sizeof(struct LinkedList));
	struct LinkedList *head = fields;	
	char * token;
	int n_elements = 1;
	char delim[2];

	delim[0] = a_delim;
	delim[1] = 0;

	token = strsep(&a_str, delim);
	fields->word = token;

	while(token){
		struct LinkedList *tmp = (struct LinkedList*)malloc(sizeof(struct LinkedList));
		token = strsep(&a_str, delim);
		tmp->word = token;
		fields->next = tmp;
		fields = fields->next;
		n_elements++;
	}

	head->n_elements = n_elements-1;

	return head;
}

int parseVCF(char *path, char *outfile){
	
	const char prefix[] = "zcat ";
	const char prefix1[] = "bgzip ";
	const char prefix2[] = "tabix -p vcf ";
	char *cmd;
	FILE *in;
	char buf[100000];
	struct LinkedList *fields;
	struct LinkedList *desc;
	struct LinkedList *effect;
	char * chr,* pos,* ref, *alt, *eff;
	FILE * out_file;
	struct stat statbuf;

	if ( access(path, F_OK) == -1 ){
		fprintf(stderr, "The input VCF doesn't exist\n");
		return 1;
	}else{
		stat(path, &statbuf);
		if(S_ISDIR(statbuf.st_mode)){
			fprintf(stderr, "The input VCF is a direcotry\n");
                	return 1;
		}
	}

	cmd = malloc(sizeof(prefix) + strlen(path) + 1);

	if(!cmd){
		fprintf(stderr, "Memory allocation failed\n");
    		return 1;
	}

	sprintf(cmd, "%s%s", prefix, path);
	
	in = popen(cmd, "r");
	
	if (!in) {
		fprintf(stderr, "The input VCF couldn't be uncompressed\n");
		return 1;
	}

	out_file = fopen(outfile, "w");

	while (fgets(buf, 100000, in)!=NULL){		

		/* Skip commented lines */
		if(buf[0] != '#'){

			fields = strsplit(buf, '\t');
			chr = get_pos(fields,0)->word;
			pos = get_pos(fields,1)->word;
			ref = get_pos(fields,3)->word;		
			alt = get_pos(fields,4)->word;

			if(*ref == 'N'){
				continue;
			}

			if(strlen(alt)>1){
				continue;
			}
 
			if(fields){

				struct LinkedList * tmp_desc;
				tmp_desc = get_pos(fields,7);

				if(strstr(tmp_desc->word,"INDEL") != NULL){
					continue;
				}

				desc = strsplit(tmp_desc->word,';');

				if(desc){
					int desc_pos=0;
					struct LinkedList * tmp_field;
					while( desc_pos < desc->n_elements){
						tmp_field  = get_pos(desc, desc_pos);
						if(strstr(tmp_field->word,"ANN") != NULL){
							effect = strsplit(tmp_field->word,'|');
							eff = get_pos(effect,2)->word;
							break;
						}
						desc_pos++;
					}
				}else{
					fprintf(stderr, "Description fields in the VCF file couldn't be splited\n");
					return 1;
				}
			}else{
				fprintf(stderr, "VCF fields couldn't be splited\n");
				return 1;
			}
			fprintf(out_file, "%s\t%s\t%s\t%s\t%s\n", chr, pos, ref, alt, eff);
		}
	}

	if (ferror(in)) {
		fprintf(stderr, "There was a problem trying to read the VCF\n");
		return 1;
	}else{
		if (!feof(in)) {
			fprintf(stderr, "Unconsumed VCF\n");
    			return 1;
  		}
	}

	fclose(out_file);
	sprintf(cmd, "%s%s", prefix1, outfile);
        system(cmd);
	sprintf(cmd, "%s%s.gz", prefix2, outfile);
        system(cmd);
	return 0;
}


int main(int argc, char *argv[]){
	
	if(argc<3){
		fprintf(stderr, "Missing parameters\n");
		return 1;
	}else{
		if(argc > 3){
			fprintf(stderr, "Unknown parameters\n");
			return 1;
		}
	}
	return parseVCF(argv[1], argv[2]);
}
