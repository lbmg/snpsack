"""
Created on 10-08-2015

@author: Diego Diaz
"""
import numpy as np

class Predictor:

    def __init__(self, kmer_list, err, use_g_indexes, output_name):
        self.kmers_list = kmer_list
        self.theta_g = None
        self.theta_i = None
        self.err = err[0]
        self.i_indexes = dict()
        self.i_orig_indexes = dict()
        self.g_indexes = dict()
        self.g_orig_indexes = dict()
        self.i_pred = dict()
        self.g_pred = dict()
        self.preds = None
        self.use_g_indexes = use_g_indexes
        self.output_name = output_name

        # parse kmer list
        for kmer_entry in self.kmers_list:

            index = float(self.kmers_list[kmer_entry][0])

            if self.use_g_indexes:
                # split kmers according if they are genic regions or not
                if self.kmers_list[kmer_entry][1] == "g":
                    self.g_orig_indexes[kmer_entry] = index
                else:
                    if self.kmers_list[kmer_entry][1] == "i":
                        self.i_orig_indexes[kmer_entry] = index
            else:
                self.i_orig_indexes[kmer_entry] = index

            # skip kmers with an index of zero (useful only to build theta values)
            if index != 0:
                if self.use_g_indexes:
                    # split kmers according if they are genic regions or not
                    if self.kmers_list[kmer_entry][1] == "g":
                        self.g_indexes[kmer_entry] = index
                    else:
                        if self.kmers_list[kmer_entry][1] == "i":
                            self.i_indexes[kmer_entry] = index
                else:
                    self.i_indexes[kmer_entry] = index

    @staticmethod
    def measure_theta(values):

        values = sorted(values)

        np_pos = np.array(list(range(1, len(values)+1)))
        np_values = np.array(values)

        possible_thetas = []

        # polynomial regression
        p = np.poly1d(np.polyfit(np_pos, np_values, 14))

        # first derivative of the polynomial regression
        p_dev = np.polyder(p)

        # second derivative of the polynomial regression
        p_2dev = np.polyder(p,2)
        p_2dev1d = np.poly1d(p_2dev)

        # third derivative of the polynomial regression
        p_3dev = np.polyder(p,3)
        p_3dev1d = np.poly1d(p_3dev)

        # roots of the first derivative
        p_dev_roots = np.roots(p_dev.coeffs).real.astype(float)

        for root in p_dev_roots:

                # check if the root value ranges between the values of the kmer indexes
                if 0 < root < np_pos[np_pos.size-1]:

                    # get the value of the root in the first derivative
                    root_2dev = p_2dev1d(root)

                    # if the value of the root in the second derivative is zero, then check the third derivative
                    if -0.00001 < root_2dev < 0.00001:
                        root_3dev = p_3dev1d(root)
                        if root_3dev != 0:
                            possible_thetas.append(root)
                    else:
                        if root_2dev > 0:
                            possible_thetas.append(root)

        return p(possible_thetas[0])

    def build_predictors(self):

        # measure theta
        #i_values = sorted(self.i_indexes.values())
        self.theta_i = self.measure_theta(self.i_indexes.values())
        if self.use_g_indexes:
            # measure theta for genomic regions
            #g_values = sorted(self.g_indexes.values())
            self.theta_g = self.measure_theta(self.g_indexes.values())
        return [self.theta_i, self.theta_g]

#import pickle
#print "loading joined indexes (for testing)"
#joined_indexes = pickle.load(open("/home/diego/work_projects/snpsack/snpsack_files/input_data/joined_indexes.p","rb"))
#print "creating the predictor"
#pred = Predictor(joined_indexes, [0], True, "cualquiercoas")
#print "Getting the predictor for genic and non-genic regions .."
#pred.build_predictors()
#print "done .."

    # def classify_kmers(self):

    #     # classify each set of kmers for genic and intergenic regions
    #     self.i_pred = self.classify_regions(self.i_orig_indexes, self.theta_i, self.err)

    #     if self.use_g_indexes:
    #         self.g_pred = self.classify_regions(self.g_orig_indexes, self.theta_g, self.err)

    # def get_kmers_classifications(self):
    #     if self.use_g_indexes:
    #         return dict(self.i_pred.items() + self.g_pred.items())
    #     else:
    #         return self.i_pred

    # def summary(self):

    #     msv = 0
    #     msv_l = 0
    #     snp = 0
    #     snp_l = 0

    #     total_kmers = len(self.i_pred) + len(self.g_pred)

    #     for kmer in self.i_pred:
    #         if self.i_pred[kmer][1] == "SNP" and self.i_pred[kmer][2] == ".":
    #             snp += 1
            
    #         if self.i_pred[kmer][1] == "MIV" and self.i_pred[kmer][2] == ".":
    #             msv += 1

    #         if self.i_pred[kmer][2] == "SNP like":
    #             snp_l += 1

    #         if self.i_pred[kmer][2] == "MIV like":
    #             msv_l += 1


    #     if self.use_g_indexes:

    #         for kmer in self.g_pred:
    #             if self.g_pred[kmer][1] == "SNP" and self.g_pred[kmer][2] == ".":
    #                 snp += 1
            
    #             if self.g_pred[kmer][1] == "MIV" and self.g_pred[kmer][2] == ".":
    #                 msv += 1

    #             if self.g_pred[kmer][2] == "SNP like":
    #                 snp_l += 1

    #             if self.g_pred[kmer][2] == "MIV like":
    #                 msv_l += 1

    #         summary = (total_kmers, snp, msv_l, msv, snp_l, self.theta_i,sorted(self.i_indexes.values()), self.theta_g, sorted(self.g_indexes.values()))

    #     else:
    #         summary = (total_kmers, snp, snp_l, msv, msv_l, self.theta_i,sorted(self.i_indexes.values()))

    #     return summary

    # @staticmethod
    # def classify_regions(kmer_list, theta, err):

    #     tmp = dict()
    #     err = float(theta)*float(err)
    #     # iterate over all kmers
    #     for kmer_coord in kmer_list:

    #         # if the kmer index is below theta is classified as SNP
    #         if kmer_list[kmer_coord] <= theta:

    #             # if the kmer index ranges between (theta-err) and theta,
    #             # then the kmer region is classified as "MIV like" too
    #             if kmer_list[kmer_coord] > (theta - err):
    #                 tmp[kmer_coord] = (kmer_list[kmer_coord], "SNP", "MIV like")
    #                 # tmp[kmer_coord] = (kmer_list[kmer_coord], theta, region, "SNP", "MIV like")

    #             else:
    #                 tmp[kmer_coord] = (kmer_list[kmer_coord], "SNP", ".")
    #                 # tmp[kmer_coord] = (kmer_list[kmer_coord], theta, region, "SNP", ".")

    #         else:
    #             # if the kmer index is above theta, then the kmer is classified as MIV
    #             if kmer_list[kmer_coord] > theta:
    #                 # if the kmer index ranges between theta and (theta + err),
    #                 # then the kmer region is classified as "SNP like" too
    #                 if kmer_list[kmer_coord] < (theta+err):
    #                     tmp[kmer_coord] = (kmer_list[kmer_coord],"MIV", "SNP like")
    #                     # tmp[kmer_coord] = (kmer_list[kmer_coord], theta,region, "MIV", "SNP like")
    #                 else:
    #                     tmp[kmer_coord] = (kmer_list[kmer_coord],"MIV", ".")
    #                     # tmp[kmer_coord] = (kmer_list[kmer_coord], theta, region, "MIV", ".")
    #     return tmp

#f = open("/Users/ddiaz/PycharmProjects/test_data/test_indexes",'r')
#kmers = dict()
#for line in f:
#    line = line.rstrip("\n")
#    fields = line.split("\t")
#    kmers[(fields[0], fields[1], fields[2])] = (fields[3],fields[4])

#pred = Predictor(kmers, [0.05], False, "pepe")
#pred.build_predictors()
#pred.make_predictions()
#pred.plot_results()
#preds = pred.get_predictions()
