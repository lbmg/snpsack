"""
Created on 10-08-2015
@author: Diego Diaz
"""

from __future__ import division
import multiprocessing
from re import sub
from sys import exit
import logging
from multiprocessing import Manager, cpu_count
from os import remove
from subprocess import call, PIPE
import cPickle as PC
from os.path import isfile
from BuildKmers import BuildKmersThread
from Predictor import Predictor
import tabix
import numpy as np
#import pickle
import math
from math import log
from operator import itemgetter


class Counter(object):
    def __init__(self):
        self.val = multiprocessing.Value('i', 0)

    def increment(self, n=1):
        with self.val.get_lock():
            self.val.value += n

    @property
    def value(self):
        return self.val.value


class PredPipeline:
    def __init__(self, args):
        self.ref_vcf_file = args.REF_INDIVIDUAL
        self.ind_vcf_file = args.INDIVIDUALS
        self.gtf_file = args.GENES
        self.genome = args.GENOME
        self.km_length = args.km_length
        self.ovp = args.ovp
        self.out_file = args.output
        self.write_log = args.write_log
        self.dil = args.dil
        self.v_level = args.v_level
        self.threads = args.processes
        self.err = args.err

        # maximum number of threads
        if self.threads > cpu_count():
            self.threads = cpu_count()

        # setup login configuration
        logging_info = {
            0: None,
            1: logging.INFO,
            2: logging.WARNING,
            3: logging.ERROR,
            4: logging.DEBUG
        }

        if logging_info[self.v_level] is not None:

            if self.write_log:
                logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s',
                                    filename='SNPSack_pred.log',
                                    filemode='w',
                                    level=logging_info[self.v_level],
                                    datefmt='%m/%d/%Y %I:%M:%S %p')
            else:
                logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s',
                                    datefmt='%m/%d/%Y %I:%M:%S %p',
                                    level=logging_info[self.v_level])
        else:
            logging.disable(logging.DEBUG)
            logging.disable(logging.INFO)
            logging.disable(logging.ERROR)
            logging.disable(logging.WARNING)

        # check if tabix and bgzip are installed
        if not self.cmd_exists("bgzip"):
            logging.error("The program bgzip is not installed ..")
            exit(1)
        if not self.cmd_exists("tabix"):
            logging.error("The program tabix is not installed ..")
            exit(1)

    @staticmethod
    def cmd_exists(cmd):
        return call("type " + cmd,
                    shell=True,
                    stdout=PIPE,
                    stderr=PIPE) == 0

    def run(self):

        process_arr = []

        logging.info("Initializing SNPSack pred pipeline ..")
        logging.info("Running the pipeline with " + str(self.threads) + " cores ..")

        indexed_ref = self.ref_vcf_file
        if not self.check_indexed_file(indexed_ref):
            exit(1)

        indexed_ind = self.ind_vcf_file
        if not self.check_indexed_file(indexed_ind):
            exit(1)

        indexed_genes = self.gtf_file
        if not self.check_indexed_file(indexed_genes):
            exit(1)
        
        # read chromosome information
        logging.info("Reading chromosome information ..")
        chr_info = self.read_chr_info(self.genome)

        # calculate global ti:tv
        logging.info("Calculating global Ti/Tv rate for MIVs ..")
        global_TiTv_val = self.global_titv(chr_info, indexed_ind)

        # split the chromosomes in different groups to build the kmer index in parallel
        logging.info("Building indexes for each kmer in the genome ..")

        divided_genome = self.task_splitter(chr_info, self.threads)

        # for each process pass a group of chromosomes as input
        cont = 1
        man = Manager()
        indexes = man.dict()
        tot_chrom = len(chr_info)
        counter = Counter()

        for i in divided_genome:
            process = BuildKmersThread("thread_" + str(cont),  # thread name
                                       i,  # ith thread
                                       indexed_ref,  # path to the indexed bgzip file of the reference individuals
                                       indexed_ind,  # path to the indexed bgzip file of the non reference individuals
                                       indexed_genes,  # path to the indexed bgzip file of the gene annotations
                                       self.km_length,  # kmer length
                                       self.ovp,  # overlap between kmers
                                       indexes,  # array where the indexes of the kmers will be stored
                                       #self.dil,  # dilation values for the first two indexes
                                       tot_chrom,
                                       counter,
                                       global_TiTv_val)
            process.start()
            process_arr.append(process)
            cont += 1

        # wait for all processes end
        for i in process_arr:
            i.join()

        ###### for debug raw indexes 
        #print "writting raw_indexes (for debug)"
        #raw_indexes = open("raw_indexes.csv","w")
        #for raw_index in indexes.keys():
        #    raw_indexes.write("\t".join(map(str, raw_index))+"\t"+"\t".join(map(str, indexes[raw_index]))+"\n")
        #################

        # normalize values
        logging.info("Normalizing indexes ..")
        normalized_indexes = self.normalize_indexes(indexes)
        ###### for debug norm indexes
        #print "writting norm_indexes (for debug)"
        #norm_indexes = open("norm_indexes.csv","w")
        #for norm_index in normalized_indexes.keys():
        #    norm_indexes.write("\t".join(map(str, norm_index))+"\t"+"\t".join(map(str, normalized_indexes[norm_index]))+"\n")
        #################

        #pickle.dump(normalized_indexes, open("indexes_test_normalized.p","wb"))

        # join indexes
        logging.info("Joining indexes ..")
        joined_indexes =  self.join_indexes(normalized_indexes, self.dil)         
        ###### for debug joined indexes
        #print "writting joined_indexes (for debug)"
        #join_indexes = open("join_indexes.csv","w")
        #for join_index in joined_indexes.keys():
        #    join_indexes.write("\t".join(map(str, join_index))+"\t"+"\t".join(map(str, joined_indexes[join_index]))+"\n")
        #################

        ##### for debug Predictor
        #pickle.dump(joined_indexes, open("joined_indexes.p", "wb"))
        #exit(0)
        ## get theta predictor
        pred = Predictor(joined_indexes, self.err, True, self.out_file+"_"+str(self.dil[0])+"_"+str(self.dil[1]))
        
        # build predictor
        logging.info("Getting the predictors ..")
        predictors = pred.build_predictors()
       
        # writting the output (kmer coords, joined_index value, region type and predictor of that regions)
        logging.info("Writting the results ..")
        output_handler = open(self.out_file,"w")
        for kmer_entry in sorted(joined_indexes.keys(), key=itemgetter(0,1)):
            
            if joined_indexes[kmer_entry][1] == "i":
                classifier = predictors[0]
            else:
                classifier = predictors[1]
        
            output_handler.write("\t".join(map(str,kmer_entry))+"\t"+"\t".join(map(str,joined_indexes[kmer_entry]))+"\t"+str(classifier)+"\n")
        output_handler.close()

        call(["bgzip", self.out_file])
        call(["tabix","-p","bed",self.out_file+".gz"])
        # annotate each kmer as MIV or SNP
        #logging.info("Annotating each kmer as MSV or SNP ..")
        #pred.classify_kmers()
        
        # retrieve annotations
        #final_preds = pred.get_kmers_classifications()

        # write predictions to a file
        #logging.info("Writing predictions to the output file ..")
        #PC.dump(final_preds, open(self.out_file, "wb"))

        # write a summary
        #logging.info("Writing a summary ..")
        # sum_file = open(self.out_file+".summary","wb")
        # sum = pred.summary()
        # sum_file.write("Total number of kmers: "+str(sum[0])+"\n")
        # sum_file.write("Number of kmer predicted as SNPs: "+str(sum[1])+"\n")
        # sum_file.write("Number of kmers predicted as SNP and MSV like: "+str(sum[2])+"\n")
        # sum_file.write("Number of kmers predicted as MSV: "+str(sum[3])+"\n")
        # sum_file.write("Number of kmers predicted as MSV and SNP like: "+str(sum[4])+"\n")
        # sum_file.write("\n")
        # sum_file.write("Kmer classifier: "+str(sum[5])+"\n")
        # sum_file.write("x_pos\ty_pos\n")
        # cont = 1
        # for val in sum[6]:
        #     sum_file.write(str(cont)+"\t"+str(val)+"\n")
        #     cont +=1
        
        # sum_file.write("\n")

        # # if genomic indexes are included
        # if len(sum) == 9:
        #     sum_file.write("Genomic kmer classifier: "+str(sum[7])+"\n")
        #     sum_file.write("x_pos\ty_pos\n")
        #     cont = 1
        #     for val in sum[8]:
        #         sum_file.write(str(cont)+"\t"+str(val)+"\n")
        #         cont +=1

        # sum_file.close()
        logging.info(str(len(joined_indexes.keys()))+" kmers generated")
        logging.info("Intergenic predictor :"+str(predictors[0]))
        logging.info("Genic predictor :"+str(predictors[1]))
        logging.info("SNPSack pred finished ..")

    @staticmethod
    def check_indexed_file(f):
        if not isfile(f):
            logging.error("The indexed file \""+f+"\" doesn't exist .. exiting")
            return False

        if not f.endswith(".gz"):
            logging.error("The indexed file \""+f+"\" doesn't end with gz")
            return False

        if not isfile(f+".tbi"):
            logging.error("The indexed file \""+f+"\" hasn't a tbi file")
            return False
        return True

    @staticmethod
    def read_chr_info(f):

        if not isfile(f):
            logging.error("The file \""+f+"\" doesn't exist .. exiting")
            exit(1)  

        chr_file = open(f, 'r')
        chr_info = dict()

        for chrom in chr_file:

            chrom = chrom.rstrip("\n")
            tmp = chrom.split("\t")
            if len(tmp) != 2:
                logging.warning("Line "+chrom+" from genome file couldn't be read, skipping it ..")
                continue

            if not tmp[1].isdigit():
                logging.warning("Chromosome "+tmp[0]+" has not a valid length in the genome file, skipping it ..")
                continue

            chr_info[tmp[0]] = int(tmp[1])

        return chr_info

    @staticmethod
    def task_splitter(chr_info, n_threads):

        # each position of this array stores a set of chromosomes that will
        # be passed to a process to build the kmer information
        divided_genome = []
        # total genome length
        genome_len = sum(chr_info.values())

        # number of nucleotides spanned by each group of chromosomes in a thread
        size_per_thread = genome_len / float(n_threads)

        # temporal python dictionary to store the group of chromosomes that will be passed to one single thread
        genome_part = dict()
        part_length = 0

        # iterate over all chromosomes
        for chromosome in chr_info:
            # length of the current chromosome
            chr_length = chr_info[chromosome]
            # we assign the current chromosome to the temporal python dictionary
            genome_part[chromosome] = chr_length
            part_length += chr_length

            # if we reached the number of nucleotides analyzed by this thread,
            # then we move to the next group of chromosomes analyzed
            # by the next thread.
            if part_length > size_per_thread:
                divided_genome.append(genome_part)
                part_length = 0
                genome_part = dict()

        divided_genome.append(genome_part)

        return divided_genome

    @staticmethod
    def global_titv(chrom_info, indexed_ind):
        
        ind_tb = tabix.open(indexed_ind)
        n_ti=0
        n_tv=0

        for chrom in chrom_info:
            
            try:

                for var in ind_tb.query(chrom, 1, int(chrom_info[chrom])):

                    if (var[2] == "A" and var[3] == "G") or (var[2] == "G" and var[3] == "A") or (
                        var[2] == "C" and var[3] == "T") or (var[2] == "T" and var[3] == "C"):
                        n_ti += 1

                    else:
                        if (var[2] == "A" and var[3] == "C") or (var[2] == "C" and var[3] == "A") or (
                            var[2] == "T" and var[3] == "G") or (var[2] == "G" and var[3] == "T") or (
                            var[2] == "T" and var[3] == "A") or (var[2] == "A" and var[3] == "T") or (
                            var[2] == "C" and var[3] == "G") or (var[2] == "G" and var[3] == "C"):
                            n_tv += 1

            except tabix.TabixError:
                pass

        return n_ti/n_tv

    @staticmethod
    def normalize_indexes(indexes):
        
        np_indexes = np.array(indexes.values())
        min_max = []
        normalized_indexes = dict()

        # get the min and max for each column
        for i in range(4):
            # get the column
            indexes_column = np_indexes[:,i]
            # remove None values
            indexes_column = [index for index in indexes_column if index != None]
            # get min and max for the current column
            min_max.append([min(indexes_column), max(indexes_column)])

        # normalize values    
        for i in indexes.keys():
            tmp = []
            indexes_array = [index for index in indexes[i] if index != None]
            for j in range(len(indexes_array)):
                tmp.append((indexes_array[j] - min_max[j][0]) / (min_max[j][1] - min_max[j][0]))
            normalized_indexes[i] = tmp
        return normalized_indexes

    @staticmethod
    def join_indexes(indexes, dil_values):
        
        joined_indexes = dict()

        # iterate over all the indexes
        for kmer in indexes:

            values = indexes[kmer]
            values[0] **= 1. / dil_values[0]
            values[1] **= 1. / dil_values[1]

            pit = 1

            for i in values:
                pit *= i

            if pit != 0:
                final_index = float(math.e ** (log(pit, math.e) / float(len(values))))
            else:
                final_index = 0

            # add region type to the final index    
            if len(indexes[kmer]) == 2:
                region_type = "i"
            else:
                region_type = "g"
            
            joined_indexes[kmer] = (final_index, region_type)

        return joined_indexes
