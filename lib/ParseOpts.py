"""
Created on 10-08-2015

@author: Diego Diaz
"""

import argparse
from sys import argv, exit
from argparse import RawTextHelpFormatter


class Range(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __eq__(self, other):
        return self.start <= other <= self.end


class ParseOpts(object):
    """
    class docs
    """

    def __init__(self):

        """
        Constructor
        :return:
        """
        parser = argparse.ArgumentParser(prog='SNPSack',
                                         description="Program:  SNPSack (Bioinformatic pipeline for variant "
                                                     "classification)\n"
                                                     "Version:  0.0.1\n"
                                                     "Contact:  Diego Diaz <ddiaz@dim.uchile.cl>",
                                         epilog="Author:  Diego Diaz <ddiaz@dim.uchile.cl> \n"
                                                "Affiliation: Center for Mathematical Modeling, University of Chile.\n"
                                                "Please cite: SNPs search and classification kit (SNPSack), 2015.\n",
                                         formatter_class=RawTextHelpFormatter)

        subparsers = parser.add_subparsers(help="sub-command help")

        prep_inputs = subparsers.add_parser('prepin',
                                             help='prepares the inputs for pred command.')

        prep_inputs._optionals.title = "arguments"

        prep_inputs.add_argument('-g', '--genes',
                                      help='GFF file with the gene annotations.',
                                      default=None,
                                      dest='genes',
                                      )
        prep_inputs.add_argument('-r', '--ref-genome',
                                      help='multi-FASTA file of the reference genome used to build the IVs.',
                                      default=None,
                                      dest='genome',
                                      )

        prep_inputs.add_argument('-m', '--mul-indivuals',
                                      help='VCF input with the multiple indivual variants (MIVs).',
                                      default=None,
                                      dest='miv',
                                      )

        prep_inputs.add_argument('-p', '--snpeff-path',
                                      help='path to the snpEff directory.',
                                      default=None,
                                      dest='snpeff_path',
                                      )

        prep_inputs.add_argument('-d', '--snpeff-database',
                                      help='snpeff database used to annotate the variants.',
                                      default=None,
                                      dest='snpeff_db',
                                      )

        prep_inputs.add_argument('-u', '--predict-utrs',
                                 dest="pred_utr",
                                 action="store_true",
                                 help='If this flag is set, then the UTR regions '
                                      'will be predicted from genes annotations.',
                                 default=False)

        prep_inputs.add_argument('-t', '--threads',
                                      help='number of threads used to build the files.',
                                      default=1,
                                      dest='threads',
                                      )

        prep_inputs.set_defaults(which="prepin")

        

        pred_parser = subparsers.add_parser('pred',
                                            help='predicts the regions in the genome affected by MSVs or PSVs.')

        pred_parser.add_argument('REF_INDIVIDUAL',
                                 help='VCF file containing the variants called in the reference individual.')

        pred_parser.add_argument('INDIVIDUALS',
                                 help='VCF file containing the variants called in the non-reference individuals.')

        pred_parser.add_argument('GENES',
                                 help='GTF|GFF|GFF3 file containing the genes annotations of the organism.')

        pred_parser.add_argument('GENOME',
                                 help='Genome file indicating the length of each chromosome.')

        pred_parser.add_argument('-l', '--kmer-length',
                                 dest='km_length',
                                 type=int,
                                 default=10000,
                                 help='Length of the kmer used to search for MSV and PSV regions (Default 10000).')

        pred_parser.add_argument('-ov', '--overlapping',
                                 dest='ovp',
                                 type=int,
                                 default=1000,
                                 help='Number of bases overlapping between consecutive kmers (Default 1000).'
                                      ' Negative values will be assumed as a separation between the kmers.')

        pred_parser.add_argument('-d', '--dilation',
                                 dest='dil',
                                 nargs=2,
                                 type=int,
                                 default=[4, 2],
                                 help='Dilation values for the first and the second indexes (Default 4,2).')


        pred_parser.add_argument('-e', '--error-fraction',
                                 dest='err',
                                 nargs=1,
                                 type=float,
                                 choices=[Range(0.0, 1.0)],
                                 metavar="[0.0-1.0]",
                                 default=[0.0],
                                 help='Error fraction allowed in the classification. '
                                      'If a given genomic region has a index below theta '
                                      'but above theta - theha*err, then the region is '
                                      'classified as SNP but also MIV like. The same occurs in the other way.'
                                      ' Allowed values range between 0.0 and 1.0 (default 0.0).')

        pred_parser.add_argument('-p', '--processes',
                                 dest="processes",
                                 type=int,
                                 default=4,
                                 help='Number of processes used to run SNPSack pred (Default 1).')

        pred_parser.add_argument('-v', '--verbose-level',
                                 dest='v_level',
                                 type=int,
                                 choices=range(0, 5),
                                 default=4,
                                 help='Verbose level. 0 for disabling verbose, 1 for info, '
                                      '2 for warnings, 3 for errors and 4 for debug purposes (Default 4).')

        pred_parser.add_argument('-lf', '--log-file',
                                 dest='write_log',
                                 default=False,
                                 action='store_true',
                                 help='If this option is set, then the verbose information is sent to a log file.')

        pred_parser.add_argument('-o', '--out-file',
                                 dest='output',
                                 default='SNPSack_predictions.p',
                                 help='Outfile with the predictions.')

        pred_parser.set_defaults(which="pred")



        intersect_parser = subparsers.add_parser('annot',
                                                 help='checks if a set of input coordinates '
                                                      'intersect with SNP or MSV regions.')

        intersect_parser.add_argument('MODEL',
                                      help='File generated by the pred command which contains '
                                           'the predicted MSV and PSV regions in the reference genome.')

        intersect_parser.add_argument('INPUT',
                                      help='VCF file with the input coordinates to be annotated.',)

        intersect_parser.add_argument('-f', '--fraction',
                                      dest="fract",
                                      type=float,
                                      default=0.9,
                                      help='Fraction of kmers intersecting a given variant below the threshold needed to declare a variant as SNP (Default 0.9).')

        intersect_parser.add_argument('-e', '--extract',
                                      help='If this flag is set, then only regions with the '
                                           'specified annotation (MSV or SNP) will be extracted.',
                                      metavar="[SNP | MSV]",
                                      choices=["SNP","MSV",None],
                                      default=None,
                                      dest='extract',
                                      )

        intersect_parser.add_argument('-o', '--out-file',
                                      help='Output file with the annotated variants.',
                                      default=None,
                                      dest='output',
                                      )

        intersect_parser.set_defaults(which="intersect")


        if len(argv) == 1:
            parser.print_help()
            exit(1)

        else:
            if len(argv) == 2:
                if argv[1] == 'pred':
                    pred_parser.print_help()
                    exit(1)

                if argv[1] == 'annot':
                    intersect_parser.print_help()
                    exit(1)

                if argv[1] == 'prepin':
                    prep_inputs.print_help()
                    exit(1)

        self.args = parser.parse_args()

    def get_opts_array(self):
        return self.args
