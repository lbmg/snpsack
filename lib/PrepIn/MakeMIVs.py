from FileParser import ParseVariants

def make_mivs(vcf_file, snpeff_path, snpeff_db):
	
	print "Annotating MIVs before creating the index .."
	
	# remove ".vcf" extension from the input vcf and replace it with "_annotation.vcf"
	tmp_array = vcf_file.split(".")
	del tmp_array[-1]
	annot_name = "".join(tmp_array)+"_annotation.vcf"

	# create a empty file with the replaced name
	annot_handler = open(annot_name,"w")

	# annotate the variants and save them in the annotation file
	call(["java","-jar", snpeff_path+"/snpEff.jar", snpeff_db, vcf_file], stdout=annot_handler)
	call(["gzip", annot_name])

	# parse the annotated variants
	pv = ParseVariants(annotated_name)

	print "Parsing annotated MIVs .."
	pv.parse()
	print "Done .."
	
	# remove the annotated variants
	call(["rm",annot_name])