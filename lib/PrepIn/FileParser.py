"""
Created on 12-08-2015

@author: Diego Diaz
"""
from collections import defaultdict

import gzip
import mimetypes
import random
import string
import sys
import os.path
import operator
import logging
from subprocess import Popen, PIPE, call
import re
import os
import io
from re import sub


class MalFormedError(Exception):
    def __init__(self, value):
        self.value = value


# arreglar esta clase, tomar solo los transcritos en un diccionario y para cada uno de ellos sacar los UTR.
class ParseGenes:

    def __init__(self, genes_path, pred_utrs):
        self.path_gtf = genes_path
        self.pred_utrs = pred_utrs

    def parse(self):
        self.parse_gff3(self.path_gtf, self.pred_utrs)

    def parse_gff3(self, path_gtf, pred_utrs):

        """
        Read a GTF/GFF3 file and store it in bgzip format.
        :param path_gtf:
        :return:
        """
        
        input_gtf = path_gtf.split("/")
        input_gtf = input_gtf[len(input_gtf)-1]

        p = None
        handle = None
        tmp_fifo = None
        gtf_entries = []

        bgzip_name = re.sub('.gz$','', input_gtf)+".bgzip.gz"
        bgzip_command = "bgzip -c > "+bgzip_name
        bgzip = Popen(bgzip_command,
                        shell=True,
                        stdin=PIPE)

        if pred_utrs:
            logging.info("UTR regions will be predicted from genes file ..")

        # features for each gene
        gene_features = defaultdict(list)

        if not os.path.isfile(path_gtf):
            logging.error("genes file \"" + path_gtf + "\" does not exist .. exiting")
            sys.exit(1)

        f_type = mimetypes.guess_type(path_gtf)

        try:
            # check if gtf file is compressed or not
            if f_type[1] is "gzip":
                tmp_fifo = "tmp_fifo"
                os.mkfifo(tmp_fifo)
                p = Popen("gzip --stdout -d "+path_gtf+" > %s" % tmp_fifo, shell=True)
                handle = io.open(tmp_fifo, "r")

            else:
                if f_type[1] is None:
                    handle = open(path_gtf, 'r')

            flag = 0

            while True:

                line = handle.readline()
                if not line:
                    break

                if line.startswith("#"):
                    continue

                line = line.rstrip("\n")
                entry = line.split("\t")

                if flag == 0:
                    prev_entry = entry
                    prev_length = len(prev_entry)
                    flag = 1

                current_length = len(entry)

                if current_length <= 1:
                    raise MalFormedError('There are no columns or they are not comma-separated')

                if prev_length != current_length:
                    raise MalFormedError('The entries have different number of columns')

                prev_length = current_length

                tmp_array = entry[8].split(";")
                gene_name = None
                feat_id = None
                parent = None

                for field in tmp_array:

                    if field.startswith("ID"):
                        feat_id = field.replace("ID=", "")

                    if field.startswith("Name"):
                        gene_name = field.replace("Name=", "")

                    if field.startswith("Parent"):
                        parent = field.replace("Parent=", "")

                if gene_name is None:
                    gene_name = parent

                tmp = (entry[0], int(entry[3]), int(entry[4]), feat_id, "0", entry[6], entry[2], parent, gene_name)

                gtf_entries.append(tmp)

                # if "predict_utr" regions is true, then store the features of the gene for predict its utr regions
                if pred_utrs:
                    if (parent is not None) and (gene_name is not None):

                        gene_features[gene_name].append([feat_id,
                                                         entry[0],
                                                         int(entry[3]),
                                                         int(entry[4]),
                                                         entry[2],
                                                         gene_name,
                                                         parent,
                                                         entry[6]])
                    else:
                        logging.warning("Feature (" + field[0]
                                        + ":"
                                        + field[3]
                                        + "-"
                                        + field[4]
                                        + ") has missing attributes. "
                                          "It will be skipped in UTR prediction")
            handle.close()

            if p is not None and tmp_fifo is not None:
                p.wait()
                os.remove(tmp_fifo)

        except IOError:
            logging.error("there was a problem trying to read \"" + path_gtf + "\" .. exiting")
            sys.exit(1)

        # predict utr regions for each gene
        if pred_utrs:
            logging.info("predicting UTR regions for genes ..")
            for gene_name in gene_features:

                utrs = self.predict_utrs(gene_features[gene_name])

                for utr_reg in utrs:

                    for utr_entries in utr_reg:
                        tmp = (utr_entries[1],
                               int(utr_entries[2]),
                               utr_entries[3],
                               utr_entries[0],
                               0,
                               utr_entries[7],
                               utr_entries[4],
                               utr_entries[6],
                               gene_name)
                        gtf_entries.append(tmp)

        gtf_entries = sorted(gtf_entries, key=operator.itemgetter(0, 1))

        for tmp in gtf_entries:
            tmp = "\t".join(map(str, tmp))+"\n"
            bgzip.stdin.write(tmp)
        bgzip.communicate()
        Popen(["tabix", "-p", "bed", bgzip_name])

    @staticmethod
    def predict_utrs(gene):
        """
        predict utr regions in a gene
        :param gene:
        :return:
        """

        exons = []
        cds = []
        mrnas = []
        f_utrs = []
        t_utrs = []

        for feature in gene:

            feature[4] = feature[4].lower()
            # group the isoforms of this gene
            if feature[4] == "mrna":
                mrnas.append(feature)

            else:
                # group the exons of this gene
                if feature[4] == "exon":
                    exons.append(feature)

                else:
                    # group the cds of this gene
                    if feature[4] == "cds":
                        cds.append(feature)

        # iterate over all transcripts of the gene
        for transcript in mrnas:

            tr_exons = []
            tr_cds = []

            utr_reg1 = []
            utr_reg2 = []

            exon_bg_cds = None
            exon_end_cds = None

            # get the cds regions for the current transcript
            for cod in cds:
                # index 6 corresponds to the parent name of this cds
                if cod[6] == transcript[0]:
                    tr_cds.append(cod)

            # sort cds regions according their coordinates
            tr_cds = sorted(tr_cds, key=operator.itemgetter(2))

            # get the cds start and end position for the current isoform
            bg_cds = tr_cds[0][2]
            end_cds = tr_cds[len(tr_cds) - 1][3]

            # get the exons of the current transcript
            for exon in exons:
                if exon[6] == transcript[0]:
                    tr_exons.append(exon)
                    # obtain in which exon the cds start is placed
                    if exon[2] <= bg_cds <= exon[3]:
                        exon_bg_cds = exon[0]  # store exon id
                    else:
                        # obtain in which exon the cds end is placed
                        if exon[2] <= end_cds <= exon[3]:
                            exon_end_cds = exon[0]  # store exon id

            # skip those transcript with missing exon information
            if exon_bg_cds is None or exon_end_cds is None:
                logging.warning("Transcript \""+transcript[0]+"\" has exon information missing .. skipping it")
                continue

            # sort exons of the current isoform
            tr_exons = sorted(tr_exons, key=operator.itemgetter(2))

            # append the first utr1 region, from the start of the transcript to the start of the first exon
            if transcript[2] < tr_exons[0][2]:
                utr_reg1.append([transcript[2], tr_exons[0][2] - 1])

            # append the exons placed before CDS start as utr1 regions
            utr1_flag = 0
            i = 0
            while utr1_flag == 0:

                tmp_bg = tr_exons[i][2]

                # if the transcript begins inside of the current exon
                if transcript[2] > tr_exons[i][2]:
                    tmp_bg = transcript[2]

                # if the current exon contain the cds start
                if tr_exons[i][0] == exon_bg_cds:
                    # we must assign as utr the segment of the exon before the cds start
                    if (bg_cds - tr_exons[i][2]) != 0:
                        utr_reg1.append([tmp_bg, bg_cds - 1])

                    utr1_flag = 1

                # if the start of the cds is not in the current exon, then this exon is utr region
                else:
                    utr_reg1.append([tmp_bg, tr_exons[i][3]])

                i += 1

            # append the exons placed after CDS end as utr2 regions
            utr2_flag = 0
            i = len(tr_exons) - 1

            while utr2_flag == 0:

                tmp_end = tr_exons[i][3]

                # if the transcript ends before the end of the current exon
                if transcript[3] < tr_exons[i][3]:
                    tmp_end = transcript[3]

                # if the current exon contain the cds end
                if tr_exons[i][0] == exon_end_cds:

                    # we must assign as utr the segment of the exon after cds end
                    if (tr_exons[i][3] - end_cds) != 0:
                        utr_reg2.append([end_cds + 1, tmp_end])

                    utr2_flag = 1

                else:
                    utr_reg2.append([tr_exons[i][2], tmp_end])

                i -= 1

            # append the last utr2 region, from the end of the last exon to the end of the transcript
            if transcript[3] > tr_exons[len(tr_exons) - 1][3]:
                utr_reg2.append([tr_exons[len(tr_exons) - 1][3] + 1, transcript[3]])

            if transcript[7] == "+":
                reg1 = "five_prime_UTR"
                reg2 = "three_prime_UTR"
                strand = "+"
            else:
                reg1 = "three_prime_UTR"
                reg2 = "five_prime_UTR"
                strand = "-"

            cont = 1

            for utr in utr_reg1:

                utr_id = reg1 + "_" + str(cont) + "_" + transcript[0]
                tmp = (utr_id, transcript[1], utr[0], utr[1], reg1, transcript[5], transcript[0], strand)
                if transcript[7] == "+":
                    f_utrs.append(tmp)
                else:
                    t_utrs.append(tmp)

                cont += 1

            cont = 1

            for utr in utr_reg2:

                utr_id = reg2 + "_" + str(cont) + "_" + transcript[0]
                tmp = (utr_id, transcript[1], utr[0], utr[1], reg2, transcript[5], transcript[0], strand)
                if transcript[7] == "+":
                    f_utrs.append(tmp)
                else:
                    t_utrs.append(tmp)

                cont += 1

        return [f_utrs, t_utrs]


class ParseVariants:

    def __init__(self, vcf_path):
        self.vcf_path = vcf_path
        tmp = self.vcf_path.split("/")
        self.output = sub('.gz$', "", tmp[len(tmp)-1])+".bgzip"

    def parse(self):
        call([os.path.dirname(os.path.realpath(__file__))+"/external/genomicParser",self.vcf_path, self.output])


    # @staticmethod
    # def parse_vcf(path_vcf):
    #
    #     """
    #     :param path_vcf:
    #     :return:
    #     """
    #
    #     input_vcf = path_vcf.split("/")
    #     input_vcf = input_vcf[len(input_vcf)-1]
    #
    #     p = None
    #     tmp_fifo = None
    #     handle = None
    #     bgzip_name = re.sub('.gz$', '', input_vcf)+".bgzip.gz"
    #     bgzip_command = "bgzip -c > "+bgzip_name
    #     # bgzip = subprocess.Popen(bgzip_command,
    #     #                          shell=True,
    #     #                          stdin=subprocess.PIPE)
    #
    #     if not os.path.isfile(path_vcf):
    #         logging.error("the VCF file \"" + path_vcf + "\" doesn't exist .. exiting")
    #         sys.exit(1)
    #
    #     f_type = mimetypes.guess_type(path_vcf)
    #     chr_lengths = dict()
    #
    #     try:
    #         if f_type[1] == "gzip":
    #
    #             #tmp_fifo = "tmp_fifo"
    #             #os.mkfifo(tmp_fifo)
    #             #p = subprocess.Popen("gzip --stdout -d "+path_vcf+" > %s" % tmp_fifo, shell=True)
    #             #handle = io.open(tmp_fifo, "r")
    #
    #             #handle = gzip.open(path_vcf, 'rb')
    #
    #             import io
    #             io_method = io.BytesIO
    #
    #             p = subprocess.Popen(["gzcat",path_vcf], stdout=subprocess.PIPE)
    #             fh = io_method(p.communicate()[0])
    #             assert p.returncode == 0
    #
    #         else:
    #             if f_type[1] is None:
    #                 handle = open(path_vcf, 'r')
    #
    #         prev_entry_len = 0
    #         flag_column = 0
    #         flag_begin = 0
    #         prev_entry = []
    #
    #         #while True:
    #
    #         #    line = handle.readline()
    #
    #         #    if not line:
    #         #        break
    #
    #         for line in fh:
    #
    #             pass
    #
    #             # line = line.rstrip("\n")
    #             # # check if the file has header at the beginning
    #             # if flag_begin == 0 and "#" not in line:
    #             #     raise MalFormedError("It does not begin with a header line")
    #             # else:
    #             #     flag_begin = 1
    #             #
    #             # if "#" in line:
    #             #     # fetch chromosome lengths
    #             #     if "##contig" in line:
    #             #         chr_info = line.split(",")
    #             #         chr_info[0] = chr_info[0].replace("##contig=<ID=", "")
    #             #         chr_info[1] = chr_info[1].replace("length=", "").replace(">", "")
    #             #         chr_lengths[chr_info[0]] = int(chr_info[1])
    #             #
    #             # else:
    #             #     entry = line.split("\t")
    #             #     # flag to fetch the first entry in the VCF file
    #             #     if flag_column == 0:
    #             #         prev_entry = entry
    #             #         prev_entry_len = len(prev_entry)
    #             #         flag_column = 1
    #             #         # check if the entries are tab-delimited
    #             #         if prev_entry_len <= 1:
    #             #             raise MalFormedError('There are no columns or they are not comma-separated')
    #             #
    #             #     # check if the entries are sorted
    #             #     if prev_entry[0] == entry[0] and int(prev_entry[1]) > int(entry[1]):
    #             #         raise MalFormedError('The VCF file does not seem to be sorted')
    #             #
    #             #     # check if all the entry rows has the same length
    #             #     entry_len = len(entry)
    #             #     if entry_len != prev_entry_len:
    #             #         raise MalFormedError('The entries have different number of columns')
    #             #
    #             #     prev_entry = entry
    #             #     prev_entry_len = entry_len
    #             #
    #             #     # discard INDELs and triallelic variants
    #             #     if "INDEL" in entry[7] or "," in entry[4]:
    #             #         continue
    #             #
    #             #     # discard N values
    #             #     if entry[3] == "N" or entry[4] == "N":
    #             #         continue
    #             #
    #             #     # check if the coordinates follow a string:integer structure
    #             #     if not entry[1].isdigit():
    #             #         raise MalFormedError("Non integer coordinates: \"" + entry[0] + ":" + entry[1] + "\"")
    #             #
    #             #     # check if the ref and alt allele follow IUPAC nomenclature (A/C/T/G)
    #             #     if entry[3] != "A" and entry[3] != "C" and entry[3] != "T" and entry[3] != "G":
    #             #         raise MalFormedError(
    #             #             "Reference allele isn't in IUPAC nomenclature at \"" + entry[0] + ":" + entry[
    #             #                 1] + "\", value:" + entry[3])
    #             #     if entry[4] != "A" and entry[4] != "C" and entry[4] != "T" and entry[4] != "G":
    #             #         raise MalFormedError(
    #             #             "Alternative allele isn't in IUPAC nomenclature at \"" + entry[0] + ":" + entry[
    #             #                 1] + "\", value: " + entry[4])
    #             #
    #             #     # get snp effect information
    #             #     variant_desc = entry[7].split(";")
    #             #     ann = None
    #             #     for field in variant_desc:
    #             #         if field.startswith("ANN"):
    #             #             ann = field.split("|")[2]
    #             #
    #             #     if ann is None:
    #             #         logging.warning("Variant (" + entry[0] + ":" + entry[1] + ") without annotation ..")
    #             #
    #             #     # save the entry in bgzip format
    #             #     bgzip_entry = [entry[0], entry[1], entry[3], entry[4], ann]
    #             #     bgzip_entry = "\t".join(bgzip_entry)+"\n"
    #             #     bgzip.stdin.write(bgzip_entry)
    #
    #         # bgzip.communicate()
    #         # subprocess.call(["tabix", "-p", "vcf", bgzip_name])
    #         handle.close()
    #
    #         if p is not None and tmp_fifo is not None:
    #             p.wait()
    #             os.remove(tmp_fifo)
    #
    #     except IOError:
    #         logging.error("There was a problem trying to read \"" + path_vcf + "\" .. exiting")
    #         sys.exit(1)
    #
    #     except MalFormedError as e:
    #         logging.error("The input VCF \"" + path_vcf + "\" is malformed [" + e.value + "] ..exiting")
    #         sys.exit(1)
    #
    #     return chr_lengths