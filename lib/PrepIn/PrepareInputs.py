from MakeIVs import make_ivs
from MakeMIVs import make_mivs
from MakeGenes import make_genes

class ParseInputs:
    def __init__(self, args):
        self.genes = args.genes
        self.genome = args.genome
        self.miv = args.miv
        self.snpeff_path = args.snpeff_path
        self.snpeff_db = args.snpeff_db
        self.threads = args.threads
        self.pred_utr = args.pred_utr

    def build(self):
        if self.genome is not None:
            make_ivs(self.genome, self.threads, self.snpeff_path, self.snpeff_db)

        if self.miv is not None:
            make_mivs(self.miv)

        if self.genes is not None:
            make_genes(self.genes, self.pred_utr)
