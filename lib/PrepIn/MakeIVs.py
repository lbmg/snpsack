__author__ = 'ddiaz'

from ruffus import *
from subprocess import call
from subprocess import PIPE
from subprocess import Popen
from Bio import SeqIO
import operator
import sys
from sys import platform as _platform
from ruffus.cmdline import MESSAGE

def split_genome_fun(genome, parts, output_dir):

	"""
	create "parts" bed files with the chunks of the genome. This bed files are then used to make tne SNP calling parallely, in order to accelerate the process.
	"""
	# we divide the genome in n sections, where n is the number of processes used. the result of the division is referenced as genome section
	g_length = sum(genome.itervalues()) # get the total length of the genome
	g_part_length = g_length/parts #we divide the genome in n sections, where n is the number of processes used. the result of the division is referenced as genome section
	n_chrom = len(genome) # obtain the number of chromosomes 
	band = True # band used to split chromosomes bigger than the genome section
	part = 1 # counter to the genome section
	acumm = 0 # acummulate size for a given section
	mod = ["none","0","0"] # if we reach the size of genome section for a certain group of chromosome, but the last chromosome has to be trimmed, then we have to add this "mod" of the last chromosome to the next file.
	chrom_cont = 0 # count for the number of chromosomes
	# iterate over all chromosomes (in increasing order)
	for chrom in sorted(genome.items(), key=operator.itemgetter(1)):
		chrom_cont +=1
		# if the chromosome is shorter than the genome section
		if chrom[1] < g_part_length:
			# open a new bed file every time we reach the size of the genome section for a given group of chromosomes
			if band:
				handler = open(output_dir+"/"+str(part)+".gchunk","w")
				band = False
			
			# if the sum of all chromosomes printed until now is bigger than the genome section, then we move to the next file
			if (acumm + chrom[1]) > g_part_length:
				
				# if we are in the last chromosome, then we can't trimm it (because we don't have any other file), then we print the complete chromosome
				if chrom_cont == n_chrom:
					end = chrom[1]

				# if we are not in the last chromosome, we trim the chromosome until reach the genome section. We save the rest of the chromosome to print it in the next file.
				else:
					end =  (g_part_length - acumm)
					mod[0] = chrom[0]
                                	mod[1] = str(end + 1)
                                	mod[2] = str(chrom[1])
                                
				# we print the last entry in the current file
				handler.write(str(chrom[0])+"\t"+str(1)+"\t"+str(end)+"\n")
				# reset all the values to open a new file
				band = True
                                acumm = 0
                                part +=1
                                handler.close()

			else:
				# check if we have to print some remaining part of a chromosome belonging to the previous file 
				if (int(mod[2]) - int(mod[1])) != 0:
					handler.write(str(mod[0])+"\t"+mod[1]+"\t"+mod[2]+"\n")
					mod = ["none","0","0"]
					acumm += int(mod[2]) - int(mod[1])
				# write the chromosome coordinates	
				handler.write(str(chrom[0])+"\t"+str(1)+"\t"+str(chrom[1])+"\n")
				acumm += chrom[1]

		# if the chromosome is bigger than the genome section
		else:
			start = 1
			# if the sum of the chromosomes printed in the previous file is shorter than the part length
			if acumm != 0 and acumm < g_part_length:
				end = g_part_lenth - acumm # check the part of the current chromosome that we have to print to reach the needed size
				handler.write(str(chrom[0])+"\t"+str(start)+"\t"+str(end)+"\n")
				handler.close()
				start = end + 1
				acumm = 0
				part += 1

			# we print a section of the chromosome that is equivalent to the genome section
			band_2 = True
			while band_2:

				handler = open(output_dir+"/"+str(part)+".gchunk","w")
				
				if (int(mod[2]) - int(mod[1])) != 0:
					handler.write(str(mod[0])+"\t"+mod[1]+"\t"+mod[2]+"\n")
					mod = ["none","0","0"]
					acumm += int(mod[2]) - int(mod[1])
					start = acumm + 1

				end = start + (g_part_length-1)
				if end <= chrom[1]:
					handler.write(str(chrom[0])+"\t"+str(start)+"\t"+str(end)+"\n")
				else:
					handler.write(str(chrom[0])+"\t"+str(start)+"\t"+str(chrom[1])+"\n")
					
				part += 1
				start = end + 1
				if start > chrom[1]:
					band_2 = False

				if ((end - start) + 1) == g_part_length:
					handler.close()
		
				else:
					acumm = end - start + 1


def generate_simulated_reads(input_file, output_file, logging):
	"""
	generate simulated reads from the input genome
	"""
	g_len = 0
	fields = input_file.split("/")
        del fields[-1]
        output_dir = "/".join(fields)
	wgsim = open(output_dir+"/wgsim.log", "w")

	for seq_record in SeqIO.parse(input_file, "fasta"):
                g_len += len(seq_record)

	# total number of bases that will be simulated (20x of coverage)
	n_pairs = (g_len * 20) / 140

	logging.log(MESSAGE,"\tGenerating simulated sequencing reads from the input genome at coverage of 20x ..")
	call(["wgsim","-N",str(n_pairs),input_file,output_file[0],output_file[1]], stdout = wgsim, stderr=wgsim)
	wgsim.close()


def map_simulated_reads(fastq_files, output_file, fasta_genome, n_threads, logging):
	"""
	map the simulated reads against the input genome
	"""
	fields = fastq_files[0].split("/")
        del fields[-1]
        output_dir = "/".join(fields)
        bwa = open(output_dir+"/bwa.log", "w")
	logging.log(MESSAGE,"\tCreating a bwa index for the input genome ..")
	call(["bwa","index",fasta_genome], stderr=bwa)
	handler = open(output_file,"w")
	logging.log(MESSAGE,"\tMapping simulated reads ..")
	call(["bwa","mem","-v","1","-t",str(n_threads),fasta_genome,fastq_files[0], fastq_files[1]], stdout=handler, stderr=bwa)
	handler.close()
	bwa.close()
 
def sam_to_bam(input_file, output_file, logging):
	"""
	convert the file with the alignments of the simulated reads from SAM to BAM format
	"""
	handler = open(input_file.replace(".sam",".bam"),"w")
	logging.log(MESSAGE,"\tConverting the alignments from SAM to BAM format ..")
	call(["samtools","view","-bS", input_file], stdout=handler)


def sort_bam(input_file, output_file, logging):
	"""
	sort the BAM file
	"""
	logging.log(MESSAGE,"\tSorting BAM file with the alignments ..")
	call(["samtools","sort",input_file,output_file])
	call(["mv",output_file+".bam",output_file])


def split_genome(input_files, genome_chunk_files, n_threads, output_dir, logging):
	"""
	generate bed file with "n_threads" genome chunks of regular size
	"""
	genome = dict()
	logging.log(MESSAGE,"\tSpliting the genome in "+str(n_threads)+" chunks of regular size to make the IVs calling ..")
	for seq_record in SeqIO.parse(input_files[0], "fasta"):
		genome[seq_record.id] = len(seq_record)
	split_genome_fun(genome, n_threads, output_dir)


def snp_calling(genome_part, output_file, bam_file, fasta_genome, logging, logger_mutex):
	"""
	make the SNP calling for each chunk of the genome
	"""
	handler = open(genome_part+"_snp_calling.log", "w")

	with logger_mutex:
		logging.log(MESSAGE,"\tMaking the SNP calling for chunk \""+genome_part+"\" ..")
	mpileup = Popen(["samtools","mpileup","-u","-l",genome_part, "-Q","10", "-q", "5", "-f", fasta_genome,bam_file], stdout=PIPE, stderr=handler)
	bcf_call = Popen(["bcftools","call","-cv","-V","indels", "-Oz", "-o", output_file], stdin= mpileup.stdout)
	mpileup.stdout.close()
	mpileup.wait()
	handler.close()

def merge_vcfs(input_vcfs, output_vcf, logging):

	"""
	merge VCF files of each genome chunk into one single VCF file
	"""

	output_vcf = output_vcf.replace(".gz","")	

	handler = open(output_vcf, "w")
	tmp = open(output_vcf+"_tmp", "w")	

	logging.log(MESSAGE,"\tJoin the variants called in each genome chunk ..")

	if _platform == "darwin":
		command = "gzcat"
	else:
		command = "zcat"

	for vcf_file in input_vcfs:
		uncompress = Popen([command,vcf_file], stdout = PIPE)
		remove_header = Popen(["grep","-v","#"], stdin = uncompress.stdout, stdout= tmp)
		uncompress.stdout.close()
		uncompress.wait()
	
	tmp.close()

	uncompress = Popen([command,input_vcfs[0]], stdout = PIPE)
	get_header = Popen(["grep", "#"], stdin= uncompress.stdout, stdout=handler)
	uncompress.stdout.close()
	uncompress.wait()	

	logging.log(MESSAGE,"\tSorting joined variants ..")
	sort = Popen(["sort","-k1,1", "-k2,2n", output_vcf+"_tmp"], stdout=handler)

	call(["rm",output_vcf+"_tmp"])
	call(["gzip", output_vcf])
	handler.close()

def annotate_variants(input_vcf, annotated_vcf, snpeff_path, snpeff_database, logging):

	"""
	annotate IVs
	"""

	fields = input_vcf.split("/")
        del fields[-1]
        output_dir = "/".join(fields)
	
	handler = open(annotated_vcf, "w")	
	snp_eff_log = open(output_dir+"/snp_eff_stderr.log", "w")

	logging.log(MESSAGE,"\tMeasuring the putative effect of the IVs ..")
	annotate = Popen(["java","-jar", snpeff_path+"/snpEff.jar", snpeff_database, input_vcf], stdout=PIPE, stderr=snp_eff_log)
	compress = Popen(["gzip"], stdin=annotate.stdout, stdout = handler)
	annotate.stdout.close()
	annotate.wait()
	call(["rm","snpEff_genes.txt","snpEff_summary.html"])

def generate_tabix_index(input_vcf, bgzip_indexed_vcf, logger):
	"""
	generate tabix index
	"""
	bgzip_indexed_vcf = bgzip_indexed_vcf.replace(".gz", "")
	logger.info("\tCreating tabix index for IVs ..")
	call(["/genoma/homes/ddiaz/SNPSack_project/LBMG-snpsack/lib/Pred/external/genomicParser", input_vcf, bgzip_indexed_vcf])
	call(["cp", bgzip_indexed_vcf+".gz", bgzip_indexed_vcf+".gz.tbi","."])

def set_pipeline_folder(output_file, logger):
	"""
	set the pipeline folder 
	"""

	fields = output_file.split("/")
	logger.info("\tCreating the working directory \""+fields[0]+"\" ..")
	call(["mkdir",fields[0]])
	logger.info("\tMaking a copy of \""+fields[1]+"\" into the working directory ..")
	call(["cp",fields[1],output_file])
	call(["samtools", "faidx", output_file])

def make_ivs(fasta_input, n_threads, snpeff_path, snpeff_database):
	
	tmp = fasta_input.split(".")
	del tmp[-1]
	print "Running the IVs pipline .."
	# ensure the "fasta" extension for the input fasta.
	fasta_copy = "".join(tmp)+".fasta"

	working_dir = "".join(tmp)+"_working_dir"

	logger, logger_mutex = cmdline.setup_logging(fasta_input+" IVs generation", "log_test", 3)

	ivs_pipeline = Pipeline(name="ivs")

	ivs_pipeline.originate( task_func	= set_pipeline_folder, 
							output		= working_dir+"/"+fasta_copy,
							extras		= [logger])	

	ivs_pipeline.transform(	task_func	= generate_simulated_reads,
							input		= set_pipeline_folder,
							filter		= suffix(".fasta"),
							output		= ["_1.fastq","_2.fastq"],
							output_dir	= working_dir,
							extras 		= [logger])

	ivs_pipeline.transform(	task_func	= map_simulated_reads,
							input		= generate_simulated_reads,
							filter		= suffix("_1.fastq"),
							output		= ".sam",
							output_dir	= working_dir,
							extras		= [working_dir+"/"+fasta_copy, n_threads, logger])

	ivs_pipeline.transform(	task_func	= sam_to_bam,
							input 		= map_simulated_reads,
							filter		= suffix(".sam"),
							output_dir	= working_dir,
							output 		= ".bam", 
							extras		= [logger])

	ivs_pipeline.transform( task_func	= sort_bam,
                            input 		= sam_to_bam,
                            filter		= suffix(".bam"),
							output_dir	= working_dir,
                            output 		= "_sorted.bam",
							extras 		= [logger])

	ivs_pipeline.split(		task_func	= split_genome,
							input  		= [working_dir+"/"+fasta_copy],
                            output  	= working_dir+'/*.gchunk',
							extras		= [n_threads, working_dir, logger]).follows(sort_bam)

	ivs_pipeline.transform( task_func	= snp_calling,
							input		= split_genome,
							filter		= suffix(".gchunk"),
							output		= ".vcf.gz",
							output_dir	= working_dir,
							extras		= [working_dir+"/"+fasta_copy.replace(".fasta","_sorted.bam"), working_dir+"/"+fasta_input, logger, logger_mutex])

	ivs_pipeline.merge( 	task_func 	= merge_vcfs,
							input		= snp_calling,
							output		= working_dir+"/"+fasta_copy.replace(".fasta", "_ivs.vcf.gz"),
							extras		= [logger])

	ivs_pipeline.transform(	task_func	= annotate_variants,
							input		= merge_vcfs,
							filter		= suffix(".vcf.gz"),
							output		= "_annotated.vcf.gz",
							output_dir	= working_dir,
							extras		= [snpeff_path,snpeff_database, logger])

	ivs_pipeline.transform( task_func	= generate_tabix_index,
							input		= annotate_variants,
							filter		= suffix("_annotated.vcf.gz"),
							output		= "_bgzip.gz",
							extras		= [logger])

	ivs_pipeline.run(multiprocess = int(n_threads), logger=logger)	
	logger.info("Pipeline finished ..")