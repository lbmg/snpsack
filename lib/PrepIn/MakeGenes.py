from FileParser import ParseGenes


def make_genes(gtf_file, pred_utr):
	parse_genes = ParseGenes(gtf_file, pred_utr)
	print "Parsing genes annotation file .."
	parse_genes.parse()
	print "Done .."