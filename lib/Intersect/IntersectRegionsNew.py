#__author__ = 'ddiaz'

from __future__ import division
from operator import itemgetter
import tabix
from mimetypes import guess_type
from sys import exit
import gzip

class IntersectRegions:

    """
    

    """

    def __init__(self, commands):

        # load the bgzip file with the model
        print "Loading model .."
        self.model = tabix.open(commands.MODEL)

        # if there is no output file, then print to the stdout
        if commands.output is not None:
            self.output_file = commands.output
        else:
            self.output_file = None

        # select which features will be kept    
        self.extract = commands.extract
        # save the input file with the variants to be classified
        self.varinputs = commands.INPUT

        #
        self.fract = commands.fract

    def parse_variants(self):

        """
        """
        model = self.model
        input_file = self.varinputs
        output_file = self.output_file
        output_handler = None
        vartype = self.extract
        fract = self.fract
        variants = []

        if output_file != None:
            output_handler = open(output_file,"w")

        # check if input_file is gzipped
        type = guess_type(input_file)[1]
            
        if type == "gzip":
                handler = gzip.open(input_file, 'r')
        else:
            handler = open(input_file, "r")

        print "Reading the variant file .."
        # iterate over each variant
        for variant in handler:
            
            is_printed = True
            # remove backslash
            variant = variant.rstrip("\n")
            # split fields (must be separated by a tab)
            variants_fields = variant.split("\t")
            # check if the row has at least 2 fields (chrom and position)
            if len(variants_fields) < 2 :
                print "the row has to have at least two fields"
                exit(1)

            # kmers which the current variant is intersecting
            try:
                below = 0 # number of kmers below the threshold
                above = 0 # number of kmers above the threshold
                kmer_indexes = []
                for kmer in model.querys(variants_fields[0]+":"+variants_fields[1]+"-"+variants_fields[1]):

                    # iterate over all the kmers that the variant is intersecting
                    if float(kmer[3]) < float(kmer[5]):
                        below +=1
                    else:
                        above +=1
                    kmer_indexes.append([kmer[3],kmer[5]])

                # get the fraction of kmers above and below the threshold
                bl_fract = below / (below + above)
                ab_fract = above / (below + above)

                if bl_fract >= fract:
                    var_class = "SNP"
                else:
                    var_class = "MSV"

                if vartype != None and vartype != var_class:
                    is_printed = False

                if is_printed:
                    if output_handler != None:
                        output_handler.write("\t".join(variants_fields)+"\t"+var_class+"\n")
                    else:
                        print "\t".join(variants_fields)+"\t"+var_class
                ## for test
                #print variants_fields[0]+":"+variants_fields[1]
                #for kmer in model.querys(variants_fields[0]+":"+variants_fields[1]+"-"+variants_fields[1]):
                #    print kmer
                #print "below_kmer_fract:"+str(bl_fract)+" min_fract: "+str(fract)
                #print var_class
                #print "\n"
                ###

            except tabix.TabixError:
                #print "Variant "+variants_fields[0]+":"+variants_fields[1]+" could not be found in the model "
                continue

