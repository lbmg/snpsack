__author__ = 'ddiaz'

from operator import itemgetter
import pickle
import gzip
from mimetypes import guess_type
from sys import exit

class IntersectRegions:

    """
    

    """

    def __init__(self, commands):
        print "Loading predictions .."
        self.predictions = pickle.load(open(commands.PREDICTIONS, "rb"))

        print "Parse variant file .."
        self.variants = self.parse_variants(commands.INPUT)
        if commands.output is not None:
            self.output_file = open(commands.output, "w")
        else:
            self.output_file = None
        self.extract = commands.extract

    @staticmethod
    def parse_variants(input_file):

        """
        It parses the input file with the info of the new variants.

        :param input_file: path to the file with the input variants
        :type input_file: string
        :returns: list with the coordinates of the input variants. 
                  Each element in the list corresponds to a variant
                  read from the input file. For each variant, the 
                  information about its chromosome, its coodinate and
                  its ID is stored.
        :rtype: python list
        """

        variants = []

        # check if input_file is gzipped
        type = guess_type(input_file)[1]
            
        if type == "gzip":
                handler = gzip.open(input_file, 'r')
        else:
                handler = open(input_file, "r")
        
        handler = open(input_file, "r")

        # iterate over each variant
        for variant in handler:
            variant = variant.rstrip("\n")
            variant = variant.split("\t")
            variants.append((variant[0], int(variant[1]), variant[2]))

        # sort variants by coordinate
        sorted_variants = sorted(variants, key=itemgetter(0,1))

        return sorted_variants

    def annotate_variants(self):

        """
        It annotates each new variants as MSV or SNP, according the snpsack predictions.
        """

        print "Annotating new variants"
        
        # counter to iterate over all the snpsack predictions.
        kmer_pos = 0

        # sort the snpsack predictions.
        kmer_coords = sorted(self.predictions.keys(), key=itemgetter(0,1))

        # iterate over all the input variants.
        for variant in self.variants:

            try:
                # iterate over the snpsack predicitons until intersect the current variant coordinate.
                while not self.is_intersected(kmer_coords[kmer_pos], variant):
                    kmer_pos += 1
            except IndexError:
                print "The variant "+str(variant[0])+":"+str(variant[1])+" is not in the chromosomes"
                exit(1)

            # annotate the current variant according the intersected snpsack prediction
            annot = self.predictions[kmer_coords[kmer_pos]][1]
            sub_annot = self.predictions[kmer_coords[kmer_pos]][2]

            # check if the variant annotation matchs with the feature type that has to be extracted.
            if (annot.find(str(self.extract)) is not -1 or sub_annot.find(str(self.extract)) is not -1)or self.extract is None :

                # check where the annotated variant has to be printed.
                if self.output_file is not None:
                    self.output_file.write(str(variant[0])+"\t" +
                                        str(variant[1])+"\t" +
                                        str(variant[2])+"\t" +
                                        annot+"\t" +
                                        sub_annot+"\n")
                else:
                    print(str(variant[0])+"\t" +
                        str(variant[1])+"\t" +
                        str(variant[2])+"\t" +
                        annot+"\t"+
                        sub_annot)

        print "Done .."

    @staticmethod
    def is_intersected(kmer_coord, variant):

        """
        It checks if a variant coordinate overlaps a genomic region annotated by snpsack pred
        
        :param kmer_coord: genomic region
        :param variant: input variant to be intersected
        :type kmer_coord: python list
        :type variant: python list
        :returns: a flag indicating if the genomic region is intersected by the variant.
        :rtype: boolean
        """

        if (kmer_coord[0] == variant[0]) and (kmer_coord[1] <= variant[1] <= kmer_coord[2]):
            return True

        return False

