"""
Created on 10-08-2015

@author: Diego Diaz
"""

from ParseOpts import ParseOpts
from Pred import PredPipeline
from Intersect import IntersectRegions
from PrepIn import ParseInputs

def run_pred(commands):
    pred = PredPipeline(commands)
    pred.run()


def run_intersect(commands):
    inter = IntersectRegions(commands)
    inter.parse_variants()


def run_prepin(commands):
    prep = ParseInputs(commands)
    prep.build()

def main():

    options = ParseOpts()
    command = options.get_opts_array()

    if command.which == "pred":
        run_pred(command)

    else:
        if command.which == "intersect":
            run_intersect(command)
        else:
            if command.which == "prepin":
                run_prepin(command)
            


if __name__ == '__main__':
    pass
    main()
