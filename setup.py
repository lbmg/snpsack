from subprocess import call
import sys

if "--user" in sys.argv:
	call(["pip", "install", "numpy","--user"])
	call(["pip", "install", "scipy","--user"])
else:
	call(["pip", "install", "numpy"])
        call(["pip", "install", "scipy"])

install_cmd = ["python", "wrapper.py"] 
install_cmd += sys.argv[1:]

call(install_cmd)
