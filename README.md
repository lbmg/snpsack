# SNPSack

SNP search and classification toolkit

## Biological background

Single Nucleotide Polymorphisms (SNPs) play a key role in genomic selection for
plant and animal breeding. Genomic selection requires a representative pool of SNPs
marking every locus of a given genome. Nevertheless, difficulties to produce an
effective pool such as recent duplication events experienced by several flowering
plants and bony fishes resulted in paralogous regions with high similarity.
This causes non-segregating variants called paralogous sequence variants (PSVs)
and affecting segregating variants present in duplicated regions (MSVs) given
the functional redundancy between duplicated sequences. Thus, it is essential to
correctly classify variants, to determine those useful for genomic selection.


![alt text](snpsack_scheme.png)


## Algorithm
SNPSack constructs a classifier for genomic regions based on the local
deviation of the transition/transversion ratio characteristic of the species and
the density of variants between paralogous sequences of a unique individual along
the genome. This classifier allows to predict a genetic variant as: PSV/MSV affected
or not PSV/MSV affected.

#### Terminology:

In order to distinguish the different sets of variants. We adopted the following terminology:

**IVs:** genetic variants obtained from mapping simulated reads to the reference genome.

**MIVs:** genetic variants obtained from mapping a set of reads generated from a group of individuals to the reference genome.

#### Commands:

SNPSack has three commands, prepin, pred and annot, each one performing a specific task.

**prepin**: prepares all the input to make the classifier.

**pred**: generates the indexes for each genome kmer and build the classifier.

**annot**: annotates new variants as ''PSV/MSV affected'' or ''SNP'' depending on the genomic region where they fall.

## Prepin

Before building a new classifier for a given specie, the input data have to be prepared. The steps performed to prepare the data are described as follows:

#### Generating IVs:
To produce IVs, SNPSack generates a set of simulated reads from the reference genome and then maps them back. This aligned reads are used to make a fake SNP calling.

#### Filtering IVs and MIVs:
Because INDELs and triallelic variants may generate conflicts with the calculation of the indexes, both are discarded as long the IVs set as the MIVs one.  

#### Predicting UTR regions:
UTR regions are used to build the genic index in kmers that overlap genes, but UTR annotations not always are available. SNPSack can infer these regions making the subtraction between CDS annotations and the transcript length.   

#### Building a Tabix index:
Retrieving the IVs, MIVs and genes annotation for each kmer in the genome may take a long time, depending on the genome size. For this reason SNPSack makes a Tabix index for the inputs, in order to accelerate the searching.

The workflow that describes all these steps is showed below:

![alt text](snpsack_prepin.png)

## Pred

![alt text](snpsack_pred.png)

## Annot

## Prerequisites
In order to run SNPSack you have to install other programs before:

- Python 2.7 >=
- Samtools
- Tabix
- Bgzip
- BWA

## Installation

SNPSack can be easily installed in any UNIX-based platform:

```bash
$ git clone https://bitbucket.org/lbmg/snpsack
$ cd snpsack
$ sudo python setup.py install
```

## Preparing the inputs to build the classifier



We wrapped all these steps in one single command to facilitate the process.  

```bash
$ snpsack prepin -r ref_genome.fasta -i ind_variants.vcf -g gene_annotations.gtf  
```
It is not necessary to pass the three inputs at the same time to the prepin command. You only have to be sure that your are passing the correct input to the flag.

## Building the classifier

```bash
$ snpsack pred ref_indexed_bgzip.gz ind_indexed_bgzip.gz genes_indexed_bgzip.bg
chromosomes.genome -p 10 -e 0.1 -o snpsack_preds
```
The result is a python object that then can be read by the annotation
command of snpsack. Each entry of this objects corresponds to a kmer region inside the genome with
its respective classification as MSV-affected or SNP.  

## Annotating new variants

The variants are annotated according the classification of the genomic region where
they falls.

The command is executed as follows:

```bash
$ snpsack annot -o snpsack_annotations snpsack_preds new_variants.vcf
```

By default, the output is a file with 3 columns, the first 2 columns of the input VCF plus
a column with the annotation. But, additionally, one can filter the variants by
annotation. For example, to annotate a set a new variants and keep only those that
were classified as SNP, you have to run the following command:

```bash
$ snpsack annot --extract SNP -o snpsack_annotations snpsack_preds new_variants.vcf
```
By the other hand, to extract only the variants that were classified as MSVs the
command has to be executed as follows:

```bash
$ snpsack annot --extract MSV -o snpsack_annotations snpsack_preds new_variants.vcf
```
