from setuptools import setup
from distutils.core import Extension

module1 = Extension('demo',
                    sources = ['readtest.c'])

setup(
    # Application name:
    name="SNPSack",

    # Version number (initial):
    version="1.2.0.dev1",

    # Application author details:
    author="Diego Diaz",
    author_email="ddiaz@dim.uchile.cl",

    # Packages
    packages=['lib', 'lib.Intersect', 'lib.Pred', 'lib.PrepIn'],

    ## Include additional files into the package
    # include_package_data=True,

    # Details
    url="https://bitbucket.org/LBMG/snpsack",

    #
    # license="LICENSE.txt",
    description="SNPs search and classification kit.",
    long_description=open("README.md").read(),

    # Dependent packages (distributions)    

    install_requires=[
        'argparse', 
	'pytabix',
	'ruffus',
	'biopython'
    ],

    entry_points={
        'console_scripts': [
            'snpsack=lib:main',
        ],
    }
)
